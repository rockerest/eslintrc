import js from "@eslint/js";
import globals from "globals";
import doThis from "eslint-plugin-do-this";

export default [
    js.configs.recommended,
    {
        languageOptions: {
            ecmaVersion: "latest",
            sourceType: "module",
            globals: {
                ...globals.browser,
                ...globals.es6,
                ...globals.node
            }
        }
    },
    {
        "plugins": {
            "do-this": doThis
        },
        "rules": {
            "do-this/no-inhuman-const": "error",
            "do-this/no-multiple-exit": "error",
            "do-this/uppercase-const": "error",
            "do-this/cyclomatic-complexity": [
                "error",
                3
            ],
    
            "accessor-pairs": "error",
            "array-bracket-spacing": [
                "error",
                "always"
            ],
            "array-callback-return": "off",
            "arrow-body-style": "off",
            "arrow-parens": "error",
            "arrow-spacing": "error",
            "block-scoped-var": "error",
            "block-spacing": [
                "error",
                "always"
            ],
            "brace-style": [
                "error",
                "stroustrup"
            ],
            "camelcase": "error",
            "comma-dangle": [
                "error",
                {
                    "functions": "never"
                }
            ],
            "comma-spacing": "off",
            "comma-style": [
                "error",
                "last"
            ],
            "complexity": "off",
            "computed-property-spacing": "off",
            "consistent-return": "off",
            "consistent-this": "off",
            "curly": "error",
            "default-case": "off",
            "dot-location": [
                "error",
                "property"
            ],
            "dot-notation": "off",
            "eol-last": "off",
            "eqeqeq": "off",
            "func-call-spacing": "error",
            "func-names": "error",
            "func-style": "off",
            "generator-star-spacing": "error",
            "guard-for-in": "off",
            "id-denylist": [
                "error",
                "data",
                "callback"
            ],
            "id-length": "off",
            "id-match": "error",
            "indent": [
                "error",
                "tab",
                {
                    "SwitchCase": 1,
                }
            ],
            "init-declarations": "off",
            "jsx-quotes": "error",
            "key-spacing": "error",
            "keyword-spacing": [
                "error",
                {
                    "overrides": {
                        "switch": {
                            "after": false
                        },
                        "if": {
                            "after": false
                        },
                        "else": {
                            "after": false
                        },
                        "do": {
                            "after": false
                        },
                        "while": {
                            "after": false
                        },
                        "for": {
                            "after": false
                        },
                        "try": {
                            "after": false
                        },
                        "catch": {
                            "after": false
                        },
                        "finally": {
                            "after": false
                        }
                    }
                }
            ],
            "linebreak-style": [
                "error",
                "unix"
            ],
            "lines-around-comment": "off",
            "max-depth": "error",
            "max-len": "off",
            "max-nested-callbacks": "error",
            "max-params": "off",
            "new-cap": "off",
            "new-parens": "error",
            "newline-after-var": "error",
            "newline-before-return": "error",
            "newline-per-chained-call": "off",
            "no-alert": "off",
            "no-array-constructor": "error",
            "no-bitwise": "off",
            "no-caller": "error",
            "no-confusing-arrow": [
                "error",
                {
                    "allowParens": true
                }
            ],
            "no-continue": "error",
            "no-debugger": "error",
            "no-div-regex": "error",
            "no-else-return": "error",
            "no-empty-function": "off",
            "no-eq-null": "error",
            "no-eval": "error",
            "no-extend-native": "off",
            "no-extra-bind": "error",
            "no-extra-label": "error",
            "no-extra-parens": "off",
            "no-floating-decimal": "error",
            "no-global-assign": "error",
            "no-implicit-globals": "error",
            "no-implied-eval": "error",
            "no-inline-comments": "off",
            "no-invalid-this": "off",
            "no-iterator": "error",
            "no-label-var": "error",
            "no-labels": "error",
            "no-lone-blocks": "error",
            "no-lonely-if": "off",
            "no-loop-func": "error",
            "no-magic-numbers": "off",
            "no-mixed-spaces-and-tabs": "error",
            "no-multi-spaces": "off",
            "no-multi-str": "error",
            "no-multiple-empty-lines": "error",
            "no-negated-condition": "off",
            "no-nested-ternary": "error",
            "no-new": "error",
            "no-new-func": "error",
            "no-new-object": "error",
            "no-new-wrappers": "error",
            "no-octal-escape": "error",
            "no-param-reassign": "off",
            "no-plusplus": "off",
            "no-proto": "error",
            "no-restricted-imports": "error",
            "no-restricted-syntax": "error",
            "no-return-assign": "off",
            "no-script-url": "error",
            "no-self-compare": "error",
            "no-sequences": "error",
            "no-shadow": "error",
            "no-shadow-restricted-names": "error",
            "no-spaced-func": "error",
            "no-ternary": "off",
            "no-throw-literal": "error",
            "no-trailing-spaces": [
                "error",
                {
                    "skipBlankLines": true
                }
            ],
            "no-undef-init": "error",
            "no-undefined": "off",
            "no-underscore-dangle": "error",
            "no-unmodified-loop-condition": "error",
            "no-unneeded-ternary": "off",
            "no-unused-expressions": "off",
            "no-use-before-define": "off",
            "no-useless-call": "off",
            "no-useless-concat": "off",
            "no-useless-constructor": "error",
            "no-var": "off",
            "no-void": "error",
            "no-warning-comments": "off",
            "no-whitespace-before-property": "error",
            "no-with": "error",
            "object-curly-spacing": [
                "error",
                "always"
            ],
            "object-shorthand": "off",
            "one-var": "off",
            "one-var-declaration-per-line": "error",
            "operator-assignment": [
                "error",
                "always"
            ],
            "operator-linebreak": "error",
            "padded-blocks": [
                "error",
                "never"
            ],
            "padding-line-between-statements": [
                "error",
                { "blankLine": "always", "prev": [ "const", "let", "var" ], "next": "*" },
                { "blankLine": "any",    "prev": [ "const", "let", "var" ], "next": [ "const", "let", "var" ] },
                { "blankLine": "always", "prev": "*", "next": "return" }
            ],
            "prefer-arrow-callback": "off",
            "prefer-const": "off",
            "prefer-rest-params": "off",
            "prefer-spread": "off",
            "prefer-template": "off",
            "quote-props": "error",
            "quotes": [
                "error",
                "double",
                "avoid-escape"
            ],
            "radix": [
                "error",
                "always"
            ],
            "require-yield": "error",
            "semi": "error",
            "semi-spacing": "off",
            "sort-imports": "off",
            "sort-vars": "error",
            "space-before-blocks": [
                "error",
                "never"
            ],
            "space-before-function-paren": [ "error", {
                "anonymous": "never",
                "named": "never",
                "asyncArrow": "always"
            } ],
            "space-in-parens": [
                "error",
                "always"
            ],
            "space-infix-ops": "error",
            "space-unary-ops": "off",
            "spaced-comment": [
                "error",
                "always"
            ],
            "strict": "off",
            "template-curly-spacing": "error",
            "vars-on-top": "error",
            "wrap-regex": "error",
            "yield-star-spacing": "error",
            "yoda": [
                "error",
                "never"
            ]
        }
    }
];